: cold
  \ https://www.xilinx.com/support/answers/69494.html
  \ https://e2e.ti.com/support/interface/f/138/t/789088
  \ Enable SGMII clock
  $4000 3 $d3 xmdio!
  \ Enable autonegotiation and reset
  $1140 3 0 mdio!
  \
  $0 3 $32 xmdio!
  \
  $0fc0 3 $14 mdio!
  \ 
  $5848 3 $10 mdio!

  \ The above starts the link, however sometimes I had to repeat 
  $1140 3 0 mdio!
;


