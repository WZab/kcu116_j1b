#!/usr/bin/env python
# This file has been minimally adapted from the original shell.py written by James Bowman
# see LICENCE.James_Bowman for details
from __future__ import print_function

import sys
import time
import array
import socket
#try:
#    import serial
#except:
#    print("This tool needs PySerial, but it was not found")
#    sys.exit(1)

sys.path.append("../original/shell")
import swapforth

class emul_ser(object):
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect(("localhost",9900))
        self.s.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, True)
        #self.s = self.s.makefile()
    def write(self,b):
        return self.s.sendall(b)
    def read(self,n):
        return self.s.recv(n)
    def flushInput(self):
        return self.flush()
    def flush(self):
        return self.s.sendall(bytearray(()))
    def inWaiting(self):
        return 0




class TetheredJ1b(swapforth.TetheredTarget):

    def open_ser(self, port, speed):
        
#        self.ser = serial.Serial(port, 115200, timeout=None, rtscts=0)
        self.ser = emul_ser()
        

    def reset(self):
        #return
        ser = self.ser
        for c in '\r\r1 tth !':
            ser.write(c.encode('utf-8'))
            ser.flush()
            time.sleep(0.001)
            ser.flushInput()
            # print(repr(ser.read(ser.inWaiting())))
        ser.write(b'\r')

        while 1:
            c = ser.read(1)
            # print repr(c)
            if c == b'\x1e':
                break

    def boot(self, bootfile = None):
        sys.stdout.write('Contacting... ')
        self.reset()
        print('established')

    def interrupt(self):
        self.reset()

    def serialize(self):
        l = self.command_response('0 here dump')
        lines = l.strip().replace('\r', '').split('\n')
        s = []
        for l in lines:
            l = l.split()
            s += [int(b, 16) for b in l[1:17]]
        s = array.array('B', s).tostring().ljust(32768, bytearray((0xff,)))
        return array.array('i', s)

if __name__ == '__main__':
    swapforth.main(TetheredJ1b)
