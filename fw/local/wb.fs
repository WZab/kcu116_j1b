$6000 constant WB_PAGE
$6100 constant WB_VAL
: wb! ( value address -- )
  WB_PAGE io!
  WB_VAL io!
;

: wb@ ( address -- value )
  WB_PAGE io!
  WB_VAL io@
;


