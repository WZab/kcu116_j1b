decimal
variable I2C_BASE
: I2C_REGS I2C_BASE @ ;

: err_halt 
    dup .
    throw
;
: i2c_master_init ( ref_freq baudrate -- )
    swap
    ( baudrate ref_freq )
    5 / 
    ( baudrate ref_freq/5 )
    swap
    ( ref_freq/5 baudrate )
    / 
    ( ref_freq/5/baudrate )
    1-
    ( ref_freq/5/baudrate-1 )
    ( divider_reg )
    dup
    ( divider_reg divider_reg )
    $ff and
    ( divider_reg divider_reg_lsb )
    I2C_REGS wb!
    ( divider_reg )
    8 rshift $ff and
    ( divider_reg_msb )
    I2C_REGS 1 + wb!
    128 I2C_REGS 2 + wb!
;

\ i2c_slv sets the address (shifted with R/W bit) of the slave
: i2c_slv ( addr -- )
    I2C_REGS 3 + wb! \ set address
    128 16 or I2C_REGS 4 + wb! \ CMD: STA+WR
    \ Wait for ACK
    begin 
	I2C_REGS 4 + wb@
	dup 2 and
    while
	    drop 
    repeat
    128 and if
	\ NACK in address
	133 err_halt
    then
;

\ i2c_wr1 writes a single byte 
: i2c_wr1 ( dta addr -- )
    2* i2c_slv
    I2C_REGS 3 + wb!
    64 16 or
    I2C_REGS 4 + wb!
    begin 
	I2C_REGS 4 + wb@
	dup 2 and
    while
	    drop 
    repeat
  128 and if
      \ NACK in data
      134 err_halt
  then
;    

: i2c_rd1 ( addr -- data )
    2* 1+ i2c_slv
    64 32 or 8 or I2C_REGS 4 + wb!
    begin 
	I2C_REGS 4 + wb@
	dup 2 and
    while
	    drop
    repeat
    drop
    I2C_REGS 3 + wb@  
;
    
    
\ i2c_wr writes multiple bytes stored in a byte array.
\ The first byte contains the length of the array.
\ Next bytes contain the data to be sent

: i2c_wr ( dtaptr addr -- )
    2* i2c_slv 
    \ Read the length of the data
    dup c@ \ dtaptr len --
    \ Now we transfer data in the loop
    begin
	swap 1+ swap \ increase dtaptr
	dup
    while
	    over c@	  
	    I2C_REGS 3 + wb!
	    1- dup if 16 else 64 16 or then 
	    I2C_REGS 4 + wb!
	    begin 
		I2C_REGS 4 + wb@
		dup 2 and
	    while
		    drop 
	    repeat
	    128 and if
		\ NACK in data
		134 err_halt
	    then
    repeat
    2drop  
;  

\ i2c_rd reads multiple bits from address addr, and stores them to the buffer
\ located at dtaptr. The number of received bytes is put in the first byte of the buffer
: i2c_rd ( dtaptr num addr -- )
    2* 1+ i2c_slv ( dtaptr num -- )
    dup 1+ 1 do ( dtaptr num -- )
	\ I from 1 to num!
	dup i = if \ last loop
	    64 32 or 8 or
	else
	    32
	then
	I2C_REGS 4 + wb!
	begin 
	    I2C_REGS 4 + wb@
	    dup 2 and
	while
		drop 
	repeat
	( dtaptr num stat )
	\ We have the status on the stack, but first we store the number of received bytes and the bytes
	rot ( stat num dtaptr )
	i over c! 
        dup i +  ( stat cnt dtaptr dest )
	I2C_REGS 3 + wb@  swap c!
	-rot
	128 and if
	    \ NACK - no more data
	    leave
	then
    loop
    drop
    drop
;
    
