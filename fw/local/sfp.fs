: i2c_select_sfp ( sfp -- )
   %/_main_ctrl.i2c_sfp_select bf!
   %/_i2c_master I2C_BASE !
;
   
decimal

: sfp_power ( sfp -- )
   i2c_select_sfp ( -- )
   104 $51 i2c_wr1 $51 i2c_rd1
   105 $51 i2c_wr1 $51 i2c_rd1
   swap 256 * +
;
