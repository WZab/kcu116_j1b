#!/bin/bash
set -e
git submodule update --init --recursive
(
  cd src
  ./gener_slaves.sh
)
(
  cd fw/original/j1b/verilator
  ./generate_kcu116_j1b_fw
)
(
  cd src/j1b/
  ./ram_init.py   mem_dump.hex
)
vivado -mode batch -notrace -nolog -nojournal -source eprj_create.tcl
vivado -mode batch -notrace -nolog -nojournal -source eprj_write.tcl
vivado -mode batch -notrace -nolog -nojournal -source eprj_build.tcl
