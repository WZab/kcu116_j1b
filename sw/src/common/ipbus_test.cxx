#include "uhal/uhal.hpp"
#include <chrono>
using namespace uhal;

ConnectionManager manager ( "file://kcu1.xml" );
HwInterface hw=manager.getDevice ( "emul" );
const Node &i2c_divl = hw.getNode("i2c_master.prer_lo");
const Node &i2c_divh = hw.getNode("i2c_master.prer_hi");
const Node &i2c_ctrl = hw.getNode("i2c_master.control");
const Node &i2c_txrx = hw.getNode("i2c_master.txr_rxr");
const Node &i2c_crsr = hw.getNode("i2c_master.cr_sr");

const int N_OF_TESTS = 100;
uint32_t test_results[N_OF_TESTS];

void i2c_init(int wb_freq, int i2c_freq)
{
    i2c_ctrl.write(0);
    int div=int(wb_freq/5/i2c_freq);
    i2c_divl.write(div & 0xff);
    i2c_divh.write((div>>8) & 0xff);
    i2c_ctrl.write(128);
    hw.dispatch();
    return;
}

int i2c_rd(int adr)
{
    i2c_txrx.write(2*adr+1);
    i2c_crsr.write(128 | 16);
    hw.dispatch();
    // Wait until bit 2 is cleared
    while(1) {
        ValWord< uint32_t > x = i2c_crsr.read();
        hw.dispatch();
        if((x.value() & 2) == 0) break;
    }
    ValWord< uint32_t > x = i2c_crsr.read();
    hw.dispatch();
    if(x.value() & 128)
        throw std::runtime_error("Error in I2C RD - no ACK");
    i2c_crsr.write(64 | 32 | 8);
    hw.dispatch();
    // Wait until bit 2 is cleared
    // Wait until bit 2 is cleared
    while(1) {
        ValWord< uint32_t > x = i2c_crsr.read();
        hw.dispatch();
        if((x.value() & 2) == 0) break;
    }
    ValWord< uint32_t > y = i2c_txrx.read();
    hw.dispatch();
    return y.value();
}

void i2c_wr(int adr, int dta)
{
    i2c_txrx.write(2*adr);
    i2c_crsr.write(128 | 16);
    hw.dispatch();
    // Wait until bit 2 is cleared
    while(1) {
        ValWord< uint32_t > x = i2c_crsr.read();
        hw.dispatch();
        if((x.value() & 2) == 0) break;
    }
    ValWord< uint32_t > x = i2c_crsr.read();
    hw.dispatch();
    if(x.value() & 128)
        throw std::runtime_error("Error in I2C WR - no adr ACK");
    i2c_txrx.write(dta);
    i2c_crsr.write(64 | 16);
    hw.dispatch();
    // Wait until bit 2 is cleared
    while(1) {
        ValWord< uint32_t > x = i2c_crsr.read();
        hw.dispatch();
        if((x.value() & 2) == 0) break;
    }
    ValWord< uint32_t > y = i2c_crsr.read();
    hw.dispatch();
    if(y.value() & 128)
        throw std::runtime_error("Error in I2C WR - no data ACK");
    return;
}

int main(int argc, char **argv)
{
    {
        i2c_init(100000000,100000);
        i2c_wr(0x74,0x8); //Outside the time measurement, because we don't want to include the time of IPbus reinitialization
        std::cout << std::hex;
        auto start = std::chrono::high_resolution_clock::now();
        i2c_wr(0x5d,0);
        for(int j=0; j<N_OF_TESTS; j++) {
            test_results[j]=i2c_rd(0x5d);
            i2c_wr(0x75,0);
        }
        auto finish = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> texec = finish-start;
        std::cout << "time:" << texec.count() << std::endl;
        for(int j=0; j<N_OF_TESTS; j++) {
            std::cout << j << ":" << test_results[j] << ",";
        }
        std::cout << std::endl;
    }
    return 0;
}
