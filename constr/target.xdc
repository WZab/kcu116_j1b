create_clock -period 3.333 -name SYSCLK_300_P -waveform {0.000 1.667} [get_ports SYSCLK_300_P]
create_generated_clock -name clk_out1_clk_wiz_0 -source [get_ports SYSCLK_300_P] -divide_by 6 [get_pins clk_wiz_0_1/clk_out1]
set_input_delay -clock [get_clocks clk_out1_clk_wiz_0] -min -add_delay 0.000 [get_ports PHY1_MDIO]
set_input_delay -clock [get_clocks clk_out1_clk_wiz_0] -max -add_delay 3.000 [get_ports PHY1_MDIO]
set_input_delay -clock [get_clocks clk_out1_clk_wiz_0] -min -add_delay 0.000 [get_ports USB_UART_TX_FPGA_RX_LS]
set_input_delay -clock [get_clocks clk_out1_clk_wiz_0] -max -add_delay 3.000 [get_ports USB_UART_TX_FPGA_RX_LS]
set_output_delay -clock [get_clocks clk_out1_clk_wiz_0] -min -add_delay 0.000 [get_ports PHY1_MDC]
set_output_delay -clock [get_clocks clk_out1_clk_wiz_0] -max -add_delay 4.000 [get_ports PHY1_MDC]
set_output_delay -clock [get_clocks clk_out1_clk_wiz_0] -min -add_delay 0.000 [get_ports PHY1_MDIO]
set_output_delay -clock [get_clocks clk_out1_clk_wiz_0] -max -add_delay 4.000 [get_ports PHY1_MDIO]
set_output_delay -clock [get_clocks clk_out1_clk_wiz_0] -min -add_delay 0.000 [get_ports USB_UART_RX_FPGA_TX_LS]
set_output_delay -clock [get_clocks clk_out1_clk_wiz_0] -max -add_delay 4.000 [get_ports USB_UART_RX_FPGA_TX_LS]

connect_debug_port u_ila_0/probe6 [get_nets [list USB_UART_RX_FPGA_TX_LS_OBUF]]
connect_debug_port u_ila_0/probe7 [get_nets [list USB_UART_TX_FPGA_RX_LS_IBUF]]



connect_debug_port u_ila_0/clk [get_nets [list clk_wiz_0_1/inst/clk_out1]]


set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk_100]
