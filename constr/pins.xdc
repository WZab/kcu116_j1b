set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property CONFIG_MODE SPIx4 [current_design]
set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLDOWN [current_design]

#set_property IOSTANDARD DIFF_SSTL15 [get_ports phy_ref_clk]
create_clock -period 1.600 -name PHY1_SGMII_CLK_P -waveform {0.000 0.800} [get_ports {PHY1_SGMII_CLK_P}]

#I2C
set_property PACKAGE_PIN AF13 [get_ports IIC_MAIN_SDA_LS]
set_property PACKAGE_PIN AE13 [get_ports IIC_MAIN_SCL_LS]
set_property IOSTANDARD LVCMOS33 [get_ports IIC_MAIN_S*]

# PHY MDIO
set_property PACKAGE_PIN P25 [get_ports PHY1_MDIO]
set_property PACKAGE_PIN U25 [get_ports PHY1_MDC]
set_property IOSTANDARD LVCMOS18 [get_ports PHY1_MD*]

# PHY SGMII
set_property PACKAGE_PIN N24 [get_ports PHY1_SGMII_IN_P]
set_property PACKAGE_PIN P24 [get_ports PHY1_SGMII_IN_N]
set_property PACKAGE_PIN U26 [get_ports PHY1_SGMII_OUT_P]
set_property PACKAGE_PIN V26 [get_ports PHY1_SGMII_OUT_N]
set_property PACKAGE_PIN T24 [get_ports PHY1_SGMII_CLK_P]
set_property PACKAGE_PIN U24 [get_ports PHY1_SGMII_CLK_N]
set_property IOSTANDARD LVDS [get_ports PHY1_SGMII_*]

set_property EQUALIZATION EQ_LEVEL0 [get_ports PHY1_SGMII_OUT_*]
set_property DQS_BIAS TRUE [get_ports PHY1_SGMII_OUT_*]
set_property DIFF_TERM_ADV TERM_100 [get_ports PHY1_SGMII_OUT_*]

set_property LVDS_PRE_EMPHASIS FALSE [get_ports PHY1_SGMII_IN_*]

# J1B UART
set_property PACKAGE_PIN W13 [get_ports USB_UART_RX_FPGA_TX_LS]
set_property PACKAGE_PIN W12 [get_ports USB_UART_TX_FPGA_RX_LS]
set_property IOSTANDARD LVCMOS33 [get_ports USB_UART_*]

# Main CLock
set_property PACKAGE_PIN K22 [get_ports SYSCLK_300_P]
set_property PACKAGE_PIN K23 [get_ports SYSCLK_300_N]
set_property IOSTANDARD LVDS [get_ports SYSCLK_300_*]


