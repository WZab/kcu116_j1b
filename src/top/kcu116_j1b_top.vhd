-------------------------------------------------------------------------------
-- Title      : J1B and IPbus on KCU116 board
-- Project    :
-------------------------------------------------------------------------------
-- File       : kcu116_j1b_top.vhd
-- Author     : Wojciech M. Zabolotny <wzab@ise.pw.edu.pl> or <wzab01@gmail.com>
-- Company    :
-- Created    : 2017-05-20
-- Last update: 2019-07-19
-- Platform   :
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2017-2019
--
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2017-05-20  1.0      wzab    Created
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;
library work;
use work.ipbus.all;
use work.wb_pkg.all;
use work.wishbone_wb_pkg.all;
use work.wishbone_pkg.all;


entity kcu116_j1b_top is
  generic(
    TEST : boolean := false
    );
  port(
    -- UART for J1B
    --USB_UART_RX_FPGA_TX_LS : out   std_logic;
    --USB_UART_TX_FPGA_RX_LS : in    std_logic;
    -- I2C
    IIC_MAIN_SDA_LS : inout std_logic;
    IIC_MAIN_SCL_LS : inout std_logic;

    -- MDIO
    PHY1_MDIO : inout std_logic;
    PHY1_MDC  : inout std_logic;

    -- SGMII
    PHY1_SGMII_IN_P  : out std_logic;
    PHY1_SGMII_IN_N  : out std_logic;
    PHY1_SGMII_OUT_P : in  std_logic;
    PHY1_SGMII_OUT_N : in  std_logic;
    PHY1_SGMII_CLK_P : in  std_logic;
    PHY1_SGMII_CLK_N : in  std_logic;

    -- System clock
    SYSCLK_300_N : in std_logic;
    SYSCLK_300_P : in std_logic
    );
end kcu116_j1b_top;

architecture beh of kcu116_j1b_top is

  constant c_gpi_num : integer := 8;
  constant c_gpo_num : integer := 4;

  signal clk   : std_logic;
  signal rst_n : std_logic;
  signal gpi_i : std_logic_vector(c_gpi_num-1 downto 0);
  signal gpo_o : std_logic_vector(c_gpo_num-1 downto 0);
  signal scio  : std_logic;

  signal clk_100, clk_200, clk_ipb, clk125_out : std_logic;
  signal rst_125                               : std_logic;
  signal sysclk_300_ib, sysclk_300_gb          : std_logic;
  signal pu_reset                              : std_logic                      := '1';
  signal rst_cnt                               : integer range 0 to 300_000_000 := 300_000_000;

  -- ipbus
  signal mac_tx_data, mac_rx_data                                                                       : std_logic_vector(7 downto 0);
  signal mac_tx_valid, mac_tx_last, mac_tx_error, mac_tx_ready, mac_rx_valid, mac_rx_last, mac_rx_error : std_logic;
  signal ipb_master_out                                                                                 : ipb_wbus;
  signal ipb_master_in                                                                                  : ipb_rbus;
  signal mac_addr                                                                                       : std_logic_vector(47 downto 0);
  signal ip_addr                                                                                        : std_logic_vector(31 downto 0);

-- WB signals as defined in wishbone_pkg
  signal wb_j1b_m2s : t_wishbone_master_out;
  signal wb_j1b_s2m : t_wishbone_master_in;
  signal wb_ipb_m2s : t_wishbone_master_out;
  signal wb_ipb_s2m : t_wishbone_master_in;
  signal wb_m_out   : t_wishbone_master_out_array(0 to 1);
  signal wb_m_in    : t_wishbone_master_in_array(0 to 1);
  signal wb_s_in    : t_wishbone_master_out_array(0 to 0);
  signal wb_s_out   : t_wishbone_master_in_array(0 to 0);

  -- WB signals defined in "lite" version
  signal wb_m2s, wb_slv_m2s : t_wb_m2s;
  signal wb_s2m, wb_slv_s2m : t_wb_s2m;

  signal wb_clk, wb_rst_0, wb_rst_1, wb_rst : std_logic := '1';

  constant c_address : t_wishbone_address_array(0 to 0) := (0 => std_logic_vector(to_unsigned(0, 32)));
  constant c_mask    : t_wishbone_address_array(0 to 0) := (0 => std_logic_vector(to_unsigned(0, 32)));

  signal status_o : std_logic_vector(15 downto 0);
  signal locked   : std_logic;

  component clk_wiz_0
    port
      (                                 -- Clock in ports
        -- Clock out ports
        clk_100   : out std_logic;
        clk_200   : out std_logic;
        clk_ipb   : out std_logic;
        -- Status and control signals
        reset     : in  std_logic;
        locked    : out std_logic;
        clk_in1_p : in  std_logic;
        clk_in1_n : in  std_logic
        );
  end component;


begin


  sysclk_buf : IBUFDS
    generic map (
      DIFF_TERM    => false,            -- Differential Termination
      IBUF_LOW_PWR => false,  -- Low power (TRUE) vs. performance (FALSE) sett
      IOSTANDARD   => "DEFAULT")
    port map (
      O  => sysclk_300_ib,              -- Buffer output
      I  => sysclk_300_p,  -- Diff_p buffer input (connect directly to top-level po
      IB => sysclk_300_n  -- Diff_n buffer input (connect directly to top-level po
      );

  sysclk_bufg : BUFG
    port map (
      O => sysclk_300_gb,               -- 1-bit output: Clock output
      I => sysclk_300_ib                -- 1-bit input: Clock input
      );

  -- Process generating the powerup reset
  purst : process (sysclk_300_gb) is
  begin  -- process purst
    if sysclk_300_gb'event and sysclk_300_gb = '1' then  -- rising clock edge
      if rst_cnt > 0 then
        rst_cnt  <= rst_cnt - 1;
        pu_reset <= '1';
      else
        pu_reset <= '0';
      end if;
    end if;
  end process purst;

  clk_wiz_0_1 : entity work.clk_wiz_0
    port map (
      clk_100 => clk_100,
      clk_200 => clk_200,
      clk_ipb => open,
      reset   => pu_reset,
      locked  => rst_n,
      clk_in1 => sysclk_300_gb);

  j1_env_1 : entity work.j1_env
    generic map (
      clk_freq_g => 100e6,
      gpi_num_g  => c_gpi_num,
      gpo_num_g  => c_gpo_num)
    port map (
      clk      => clk_100,
      rst_n    => rst_n,
      gpi_i    => gpi_i,
      gpo_o    => gpo_o,
--      uart_tx  => USB_UART_RX_FPGA_TX_LS,
--      uart_rx  => USB_UART_TX_FPGA_RX_LS,
      mdio     => PHY1_MDIO,
      mdc      => PHY1_MDC,
      wb_m_out => wb_j1b_m2s,
      wb_m_in  => wb_j1b_s2m);


-- ipbus control logic
  mac_addr <= X"020ddba11599";  -- Careful here, arbitrary addresses do not always work
  ip_addr  <= X"0a0101c0";              -- 10.1.1.192

  eth_usp_sgmii_wrapper_1 : entity work.eth_usp_sgmii_wrapper
    port map (
      clk2        => clk_200,
      gt_clkp     => PHY1_SGMII_CLK_P,
      gt_clkn     => PHY1_SGMII_CLK_N,
      gt_txp      => PHY1_SGMII_IN_P,
      gt_txn      => PHY1_SGMII_IN_N,
      gt_rxp      => PHY1_SGMII_OUT_P,
      gt_rxn      => PHY1_SGMII_OUT_N,
      clk125_out  => clk125_out,
      rsti        => not rst_n,
      rst_out     => rst_125,
      locked      => locked,
      tx_data     => mac_tx_data,
      tx_valid    => mac_tx_valid,
      tx_last     => mac_tx_last,
      tx_error    => mac_tx_error,
      tx_ready    => mac_tx_ready,
      rx_data     => mac_rx_data,
      rx_valid    => mac_rx_valid,
      rx_last     => mac_rx_last,
      rx_error    => mac_rx_error,
      status_o    => status_o,
      hostbus_in  => open,
      hostbus_out => open);

  ipbus : entity work.ipbus_ctrl
    port map(
      mac_clk      => clk125_out,
      rst_macclk   => rst_125,          -- @!@
      ipb_clk      => clk125_out,
      rst_ipb      => rst_125,          -- @!@
      mac_rx_data  => mac_rx_data,
      mac_rx_valid => mac_rx_valid,
      mac_rx_last  => mac_rx_last,
      mac_rx_error => mac_rx_error,
      mac_tx_data  => mac_tx_data,
      mac_tx_valid => mac_tx_valid,
      mac_tx_last  => mac_tx_last,
      mac_tx_error => mac_tx_error,
      mac_tx_ready => mac_tx_ready,
      ipb_out      => ipb_master_out,
      ipb_in       => ipb_master_in,
      mac_addr     => mac_addr,
      ip_addr      => ip_addr,
      pkt_rx       => open,
      pkt_tx       => open,
      pkt_rx_led   => open,
      pkt_tx_led   => open
      );
-- Conversion of IPbus signals to WB signals (sufficient fo classic single accesses)
  wb_ipb_m2s.dat <= ipb_master_out.ipb_wdata;
  wb_ipb_m2s.adr <= ipb_master_out.ipb_addr;
  wb_ipb_m2s.cyc <= ipb_master_out.ipb_strobe;
  wb_ipb_m2s.stb <= ipb_master_out.ipb_strobe;
  wb_ipb_m2s.sel <= (others => '1');
  wb_ipb_m2s.we  <= ipb_master_out.ipb_write;

  ipb_master_in.ipb_rdata <= wb_ipb_s2m.dat;
  ipb_master_in.ipb_ack   <= wb_ipb_s2m.ack;
  ipb_master_in.ipb_err   <= wb_ipb_s2m.err;

  wb_j1b_s2m  <= wb_m_in(0);
  wb_m_out(0) <= wb_j1b_m2s;

  wb_clk <= clk_100;

  process (wb_clk, rst_n) is
  begin  -- process
    if rst_n = '0' then                 -- asynchronous reset (active low)
      wb_rst_0 <= '1';
      wb_rst_1 <= '1';
      wb_rst   <= '1';
    elsif wb_clk'event and wb_clk = '1' then  -- rising clock edge
      wb_rst_0 <= '0';
      wb_rst_1 <= wb_rst_0;
      wb_rst   <= wb_rst_1;
    end if;
  end process;

  wb_cdc_1 : entity work.wb_cdc
    generic map (
      width => 32)
    port map (
      slave_clk_i    => clk125_out,
      slave_rst_n_i  => not rst_125,
      slave_i        => wb_ipb_m2s,
      slave_o        => wb_ipb_s2m,
      master_clk_i   => wb_clk,
      master_rst_n_i => not wb_rst,
      master_i       => wb_m_in(1),
      master_o       => wb_m_out(1));

  main_1: entity work.main
    port map (
      rst_n_i   => not wb_rst,
      clk_sys_i => wb_clk,
      wb_s_in   => wb_m_out,
      wb_s_out  => wb_m_in,
      i2c_scl   => IIC_MAIN_SCL_LS,
      i2c_sda   => IIC_MAIN_SDA_LS);
  
  wb_s_out(0) <= wb2wishbone_s2m(wb_slv_s2m);
  wb_slv_m2s  <= wishbone2wb_m2s(wb_s_in(0));


end beh;
