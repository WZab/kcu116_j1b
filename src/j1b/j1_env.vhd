-------------------------------------------------------------------------------
-- Title      : Environment for J1B working in the GBTxEMU emulator
-- Project    : 
-------------------------------------------------------------------------------
-- File       : j1_env.vhd
-- Author     : Wojciech M. Zabolotny  <wzab01@gmail.com>
-- Company    :
-- License    : BSD License
-- Created    : 2016-07-07
-- Last update: 2019-07-11
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2016 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2016-07-07  1.0      wzab    Created
-- 2018-11-29  1.1      wzab    Modified for GBTxEMU
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use std.textio.all;
library work;
use work.wishbone_pkg.all;
use work.ram_prog.all;
-------------------------------------------------------------------------------

entity j1_env is
  generic (
    clk_freq_g : natural := 100e6;
    gpi_num_g  : natural := 0;
    gpo_num_g  : natural := 0
    );
  port (
    clk   : in std_logic;
    rst_n : in std_logic;

    gpi_i : in  std_logic_vector(gpi_num_g-1 downto 0);
    gpo_o : out std_logic_vector(gpo_num_g-1 downto 0);

    --uart_tx  : out   std_logic;
    --uart_rx  : in    std_logic;
    mdio     : inout std_logic;
    mdc      : out   std_logic;
    wb_m_out : out   t_wishbone_master_out;
    wb_m_in  : in    t_wishbone_master_in
    );

end entity j1_env;

-------------------------------------------------------------------------------

architecture test of j1_env is

  constant uart_div_c : natural := (clk_freq_g/115200)/16;

  -- Constants for address definitions
  -- (Please note, that if you modify them, the Forth procedures
  -- may need readjsutment!

  constant UART_DATA   : integer := 16#1000#;
  constant TIMER_FREQ  : integer := 16#1010#;
  constant TIMER_LSW   : integer := 16#1014#;
  constant TIMER_MSW   : integer := 16#1018#;
  constant TIMER_MS    : integer := 16#101c#;
  constant MDIO_REG    : integer := 16#1020#;
  constant SCIO_REG    : integer := 16#1030#;  -- Two registers!
  constant UART_STATUS : integer := 16#2000#;
  constant GPI_ADDR    : integer := 16#100#;
  constant GPO_ADDR    : integer := 16#101#;

  -- Access to the WB controller - two 256-word pages.
  constant JWB_REGS_PAGE : integer := 16#60#;
  constant JWB_DATA_PAGE : integer := 16#61#;


  component j1 is
    generic (
      WIDTH : integer);
    port (
      clk       : in  std_logic;
      resetq    : in  std_logic;
      io_rd     : out std_logic;
      io_wr     : out std_logic;
      io_ready  : in  std_logic;
      mem_addr  : out unsigned(15 downto 0);
      mem_wr    : out std_logic;
      dout      : out std_logic_vector(WIDTH-1 downto 0);
      mem_din   : in  std_logic_vector(WIDTH-1 downto 0);
      io_din    : in  std_logic_vector(WIDTH-1 downto 0);
      code_addr : out unsigned(12 downto 0);
      insn      : in  std_logic_vector(15 downto 0));
  end component j1;

  component j1b2wb is
    generic (
      ADRWIDTH  : integer;
      DATAWIDTH : integer);
    port (
      J1B_CLK      : in  std_logic;
      J1B_ARESETN  : in  std_logic;
      J1B_IO_RD    : in  std_logic;
      J1B_IO_WR    : in  std_logic;
      J1B_IO_READY : out std_logic;
      J1B_IO_ADDR  : in  std_logic_vector(15 downto 0);
      J1B_DOUT     : in  std_logic_vector(31 downto 0);
      J1B_DIN      : out std_logic_vector(31 downto 0);
      J1B_WB_DATA  : in  std_logic;
      J1B_WB_REGS  : in  std_logic;
      wb_clk_i     : in  std_logic;
      wb_rst_i     : in  std_logic;
      wb_addr_o    : out std_logic_vector(31 downto 0);
      wb_dat_o     : out std_logic_vector(31 downto 0);
      wb_we_o      : out std_logic;
      wb_sel_o     : out std_logic_vector(3 downto 0);
      wb_stb_o     : out std_logic;
      wb_cyc_o     : out std_logic;
      wb_dat_i     : in  std_logic_vector(31 downto 0);
      wb_err_i     : in  std_logic;
      wb_ack_i     : in  std_logic);
  end component j1b2wb;

  component uart is
    generic (
      brg_div : integer);
    port (
      clk         : in  std_logic;
      din         : in  std_logic_vector(7 downto 0);
      dout        : out std_logic_vector(7 downto 0);
      rx_rd       : in  std_logic;
      tx_wr       : in  std_logic;
      nrst        : in  std_logic;
      tx_empty    : out std_logic;
      tx_finished : out std_logic;
      rx_full     : out std_logic;
      fr_err      : out std_logic;
      ovr_err     : out std_logic;
      tx          : out std_logic;
      rx          : in  std_logic);
  end component uart;

-- component ports
  signal uart_rd, uart_wr                 : std_logic;
  signal uart_din, uart_dout              : std_logic_vector(7 downto 0);
  signal uart_dav, uart_ready, uart_empty : std_logic;

  signal code_addr    : unsigned(12 downto 0);
  signal dout, dout_d : std_logic_vector(31 downto 0);
  signal insn         : std_logic_vector(15 downto 0);

  signal io_din, mem_din : std_logic_vector(31 downto 0);
  signal io_rd, io_rd_d  : std_logic;
  signal io_wr, io_wr_d  : std_logic;

  --mem_addr is used for memory access if strobed with mem_wr
  -- and for io access if strobed with io_rd/io_wr
  signal mem_addr, mem_addr_d : unsigned(15 downto 0);
  signal io_addr_d            : unsigned(15 downto 0);
  signal mem_wr               : std_logic;
  signal io_ready             : std_logic;
  signal rst_p                : std_logic                     := '1';
  signal sv_mem_addr          : std_logic_vector(15 downto 0) := (others => '0');

  -- Internal Wishbone controller and bus

  signal jwb_data, jwb_regs, jwb_ready : std_logic;
  signal jwb_dout                      : std_logic_vector(31 downto 0);

  signal wb_clk_i : std_logic;
  signal wb_rst_i : std_logic;

  -- Program and data memory in form which can be inferred by Vivado

  shared variable ram : T_RAM_PROG := ram_init;
  signal codeaddr     : unsigned(12 downto 0);
  signal ram_data     : std_logic_vector(31 downto 0);
  signal code_sel     : std_logic;

  -- Timers and counters used by swapforth
  signal timer_cnt     : unsigned(63 downto 0)         := (others => '0');
  signal timer_latch   : std_logic_vector(63 downto 0) := (others => '0');
  signal counter_ms    : unsigned(31 downto 0)         := (others => '0');
  signal counter_subms : unsigned(31 downto 0)         := (others => '0');

  signal timer_freq_wr : std_logic := '0';

  -- Ethernet MDIO controller
  signal mdio_register : std_logic_vector(31 downto 0) := (others => '0');
  signal mdio_reg_wr   : std_logic;

  -- GPIO output register
  signal reg_gpo_o    : std_logic_vector(gpo_num_g-1 downto 0) := (others => '0');
  signal gpo_o_reg_wr : std_logic;

begin  -- architecture test

  wb_clk_i <= clk;
  wb_rst_i <= rst_n;

  rst_p <= not rst_n;

  -- MG
  -- seems that j1 uses byte addressing
  -- code addr takes 16 bit data (on signal insn) so the ram address must be shifted once
  codeaddr <= '0' & code_addr(12 downto 1);

  -- byte 0 of the address is stored in register
  insn_byte_sel : process (clk) is
  begin
    if clk'event and clk = '1' then
      code_sel <= code_addr(0);
    end if;
  end process;

  -- 32 bit data is read from the memory
  ram_port_rd : process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      ram_data <= ram(to_integer(unsigned(codeaddr)));
    end if;
  end process;

  -- the right bytes (LSB or MSB) is selected with code_sel afterwords
  insn <= ram_data(31 downto 16) when code_sel = '1' else ram_data(15 downto 0);


  -- read/write port 
  ram_port_rw : process (clk) is
  begin  -- process
    if clk'event and clk = '1' then     -- rising clock edge
      if mem_wr = '1' then
        ram(to_integer(unsigned(mem_addr(14 downto 2)))) := dout;
      end if;
      mem_din <= ram(to_integer(unsigned(mem_addr(14 downto 2))));
    end if;
  end process;



  -- I/O service
  io_flags_sp : process(clk) is
  begin
    if clk'event and clk = '1' then     -- rising clock edge
      io_rd_d <= io_rd;
      io_wr_d <= io_wr;
      dout_d  <= dout;
      if io_wr = '1' or io_rd = '1' then
        io_addr_d <= mem_addr;
      end if;
    end if;
  end process;

  -- if the MSB of the mem_addr is equal to JWB_REGS_PAGE or JWB_DATA_PAGE generate jwb strobe
  jwb_regs <= '1' when mem_addr(15 downto 8) = to_unsigned(JWB_REGS_PAGE, 8) else '0';
  jwb_data <= '1' when mem_addr(15 downto 8) = to_unsigned(JWB_DATA_PAGE, 8) else '0';

  sv_mem_addr <= std_logic_vector(mem_addr);

  j1b2wb_1 : j1b2wb
    generic map (
      ADRWIDTH  => 0,  -- The whole address goes from register!
      DATAWIDTH => 32)
    port map (
      J1B_CLK      => clk,
      J1B_ARESETN  => rst_n,
      J1B_IO_RD    => io_rd,
      J1B_IO_WR    => io_wr,
      J1B_IO_READY => jwb_ready,
      J1B_IO_ADDR  => sv_mem_addr,
      J1B_DOUT     => dout,
      J1B_DIN      => jwb_dout,
      J1B_WB_DATA  => jwb_data,
      J1B_WB_REGS  => jwb_regs,
      wb_clk_i     => wb_clk_i,
      wb_rst_i     => wb_rst_i,
      wb_addr_o    => wb_m_out.adr,
      wb_dat_o     => wb_m_out.dat,
      wb_we_o      => wb_m_out.we,
      wb_sel_o     => wb_m_out.sel,
      wb_stb_o     => wb_m_out.stb,
      wb_cyc_o     => wb_m_out.cyc,
      wb_dat_i     => wb_m_in.dat,
      wb_err_i     => wb_m_in.err,
      wb_ack_i     => wb_m_in.ack
      );

  -- j1b data input
  -- UART or WB bridge
  io_din <= x"000000" & uart_dout when io_addr_d = to_unsigned(UART_DATA, 16) else
            (0      => uart_ready, 1 => uart_dav, others => '0') when io_addr_d = to_unsigned(UART_STATUS, 16) else
            timer_latch(31 downto 0)                             when io_addr_d = to_unsigned(TIMER_LSW, 16) else
            timer_latch(63 downto 32)                            when io_addr_d = to_unsigned(TIMER_MSW, 16) else
            std_logic_vector(to_unsigned(clk_freq_g, 32))        when io_addr_d = to_unsigned(TIMER_FREQ, 16) else
            std_logic_vector(counter_ms)                         when io_addr_d = to_unsigned(TIMER_MS, 16) else
            std_logic_vector(resize(unsigned(gpi_i), 32))        when io_addr_d = to_unsigned(GPI_ADDR, 16) else
            std_logic_vector(resize(unsigned(reg_gpo_o), 32))    when io_addr_d = to_unsigned(GPO_ADDR, 16) else
            (0      => mdio, others => '0')                      when io_addr_d = to_unsigned(MDIO_REG, 16) else
            jwb_dout                                             when (jwb_regs = '1' or jwb_data = '1') else
            (others => '0');

  io_ready <= jwb_ready when (jwb_regs = '1' or jwb_data = '1') else
              '1';

  process (clk, rst_n) is
    constant c_subms_limit : integer := clk_freq_g/1000;
  begin  -- process
    if rst_n = '0' then                 -- asynchronous reset (active low)
      counter_ms    <= (others => '0');
      counter_subms <= (others => '0');
      timer_cnt     <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if counter_subms = c_subms_limit-1 then
        counter_subms <= (others => '0');
        counter_ms    <= counter_ms + 1;
      else
        counter_subms <= counter_subms + 1;
      end if;
      timer_cnt <= timer_cnt + 1;
      if timer_freq_wr = '1' then
        timer_latch <= std_logic_vector(timer_cnt);
      end if;
    end if;
  end process;

  timer_freq_wr <= '1' when io_wr_d = '1' and io_addr_d = to_unsigned(TIMER_FREQ, 16) else '0';


  process (clk, rst_n) is
  begin  -- process
    if rst_n = '0' then                 -- asynchronous reset (active low)
      reg_gpo_o <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if gpo_o_reg_wr = '1' then
        reg_gpo_o <= dout_d(gpo_num_g-1 downto 0);
      end if;
    end if;
  end process;
  gpo_o_reg_wr <= '1' when io_wr_d = '1' and io_addr_d = to_unsigned(GPO_ADDR, 16) else '0';
  gpo_o        <= reg_gpo_o;

  process (clk, rst_n) is
  begin  -- process
    if rst_n = '0' then                 -- asynchronous reset (active low)
      mdio_register <= (others => '0');
    elsif clk'event and clk = '1' then  -- rising clock edge
      if mdio_reg_wr = '1' then
        mdio_register <= dout_d;
      end if;
    end if;
  end process;

  mdio        <= 'Z' when mdio_register(1) = '0'                                  else mdio_register(0);
  mdc         <= mdio_register(2);
  mdio_reg_wr <= '1' when io_wr_d = '1' and io_addr_d = to_unsigned(MDIO_REG, 16) else '0';


  -- component instantiation
  DUT : j1
    generic map (
      WIDTH => 32)
    port map (
      clk       => clk,
      --this is actually active low!!
      resetq    => rst_n,
      io_rd     => io_rd,
      io_wr     => io_wr,
      io_ready  => io_ready,
      io_din    => io_din,
      dout      => dout,
      mem_wr    => mem_wr,
      mem_din   => mem_din,
      mem_addr  => mem_addr,
      code_addr => code_addr,
      insn      => insn
      );

  uart_wr  <= '1' when io_wr_d = '1' and io_addr_d = to_unsigned(UART_DATA, 16) else '0';
  uart_rd  <= '1' when io_rd_d = '1' and io_addr_d = to_unsigned(UART_DATA, 16) else '0';
  uart_din <= dout_d(7 downto 0);

  jtag_uart_1 : entity work.jtag_uart
    port map (
      din   => uart_din,
      dout  => uart_dout,
      wr    => uart_wr,
      rd    => uart_rd,
      clk   => clk,
      rst_n => rst_n,
      rdy   => uart_ready,
      dav   => uart_dav);


end architecture test;


