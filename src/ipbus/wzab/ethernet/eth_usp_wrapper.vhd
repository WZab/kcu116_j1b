-- This file instantiates the Xilinx SGMII PCS/PMA block and conects it to the eth_usp_gmii
-- It is based on original eth_7s_1000basex written by Dave Newbold

library ieee;
use ieee.std_logic_1164.all;

library unisim;
use unisim.VComponents.all;
use work.emac_hostbus_decl.all;

entity eth_usp_sgmii_wrapper is
  port(
    clk2             : in  std_logic;
    gt_clkp, gt_clkn : in  std_logic;
    gt_txp, gt_txn   : out std_logic;
    gt_rxp, gt_rxn   : in  std_logic;
    clk125_out       : out std_logic;
    rsti             : in  std_logic;
    rst_out          : out std_logic;
    locked           : out std_logic;
    tx_data          : in  std_logic_vector(7 downto 0);
    tx_valid         : in  std_logic;
    tx_last          : in  std_logic;
    tx_error         : in  std_logic;
    tx_ready         : out std_logic;
    rx_data          : out std_logic_vector(7 downto 0);
    rx_valid         : out std_logic;
    rx_last          : out std_logic;
    rx_error         : out std_logic;
    status_o         : out std_logic_vector(15 downto 0);
    hostbus_in       : in  emac_hostbus_in := ('0', "00", "0000000000", X"00000000", '0', '0', '0');
    hostbus_out      : out emac_hostbus_out
    );

end eth_usp_sgmii_wrapper;

architecture rtl of eth_usp_sgmii_wrapper is

  component eth_usp_gmii is
    port (
      clk125      : in  std_logic;
      rst         : in  std_logic;
      --gmii_gtx_clk : out std_logic;
      gmii_txd    : out std_logic_vector(7 downto 0);
      gmii_tx_en  : out std_logic;
      gmii_tx_er  : out std_logic;
      gmii_rx_clk : in  std_logic;
      gmii_rxd    : in  std_logic_vector(7 downto 0);
      gmii_rx_dv  : in  std_logic;
      gmii_rx_er  : in  std_logic;
      tx_data     : in  std_logic_vector(7 downto 0);
      tx_valid    : in  std_logic;
      tx_last     : in  std_logic;
      tx_error    : in  std_logic;
      tx_ready    : out std_logic;
      rx_data     : out std_logic_vector(7 downto 0);
      rx_valid    : out std_logic;
      rx_last     : out std_logic;
      rx_error    : out std_logic;
      hostbus_in  : in  emac_hostbus_in := ('0', "00", "0000000000", X"00000000", '0', '0', '0');
      hostbus_out : out emac_hostbus_out);
  end component eth_7s_gmii;

  component gig_ethernet_pcs_pma_0
    port (
      sgmii_clk_r_0          : out std_logic;
      sgmii_clk_f_0          : out std_logic;
      sgmii_clk_en_0         : out std_logic;
      clk125_out             : out std_logic;
      clk312_out             : out std_logic;
      rst_125_out            : out std_logic;
      refclk625_n            : in  std_logic;
      refclk625_p            : in  std_logic;
      speed_is_10_100_0      : in  std_logic;
      speed_is_100_0         : in  std_logic;
      reset                  : in  std_logic;
      txn_0                  : out std_logic;
      rxn_0                  : in  std_logic;
      gmii_txd_0             : in  std_logic_vector(7 downto 0);
      gmii_rxd_0             : out std_logic_vector(7 downto 0);
      txp_0                  : out std_logic;
      gmii_rx_dv_0           : out std_logic;
      gmii_rx_er_0           : out std_logic;
      gmii_isolate_0         : out std_logic;
      rxp_0                  : in  std_logic;
      signal_detect_0        : in  std_logic;
      gmii_tx_en_0           : in  std_logic;
      gmii_tx_er_0           : in  std_logic;
      configuration_vector_0 : in  std_logic_vector(4 downto 0);
      status_vector_0        : out std_logic_vector(15 downto 0);
      an_adv_config_vector_0 : in  std_logic_vector(15 downto 0);
      an_restart_config_0    : in  std_logic;
      an_interrupt_0         : out std_logic;
      tx_dly_rdy_1           : in  std_logic;
      rx_dly_rdy_1           : in  std_logic;
      tx_vtc_rdy_1           : in  std_logic;
      rx_vtc_rdy_1           : in  std_logic;
      tx_dly_rdy_2           : in  std_logic;
      rx_dly_rdy_2           : in  std_logic;
      tx_vtc_rdy_2           : in  std_logic;
      rx_vtc_rdy_2           : in  std_logic;
      tx_dly_rdy_3           : in  std_logic;
      rx_dly_rdy_3           : in  std_logic;
      tx_vtc_rdy_3           : in  std_logic;
      rx_vtc_rdy_3           : in  std_logic;
      tx_logic_reset         : out std_logic;
      rx_logic_reset         : out std_logic;
      rx_locked              : out std_logic;
      tx_locked              : out std_logic;
      tx_bsc_rst_out         : out std_logic;
      rx_bsc_rst_out         : out std_logic;
      tx_bs_rst_out          : out std_logic;
      rx_bs_rst_out          : out std_logic;
      tx_rst_dly_out         : out std_logic;
      rx_rst_dly_out         : out std_logic;
      tx_bsc_en_vtc_out      : out std_logic;
      rx_bsc_en_vtc_out      : out std_logic;
      tx_bs_en_vtc_out       : out std_logic;
      rx_bs_en_vtc_out       : out std_logic;
      riu_clk_out            : out std_logic;
      riu_wr_en_out          : out std_logic;
      tx_pll_clk_out         : out std_logic;
      rx_pll_clk_out         : out std_logic;
      tx_rdclk_out           : out std_logic;
      riu_addr_out           : out std_logic_vector(5 downto 0);
      riu_wr_data_out        : out std_logic_vector(15 downto 0);
      riu_nibble_sel_out     : out std_logic_vector(1 downto 0);
      rx_btval_1             : out std_logic_vector(8 downto 0);
      rx_btval_2             : out std_logic_vector(8 downto 0);
      rx_btval_3             : out std_logic_vector(8 downto 0);
      riu_valid_3            : in  std_logic;
      riu_valid_2            : in  std_logic;
      riu_valid_1            : in  std_logic;
      riu_prsnt_1            : in  std_logic;
      riu_prsnt_2            : in  std_logic;
      riu_prsnt_3            : in  std_logic;
      riu_rddata_3           : in  std_logic_vector(15 downto 0);
      riu_rddata_1           : in  std_logic_vector(15 downto 0);
      riu_rddata_2           : in  std_logic_vector(15 downto 0)
      );
  end component;

  component vio_0
    port (
      clk        : in  std_logic;
      probe_in0  : in  std_logic_vector(15 downto 0);
      probe_in1  : in  std_logic_vector(0 downto 0);
      probe_out0 : out std_logic_vector(0 downto 0)
      );
  end component;

  signal gmii_txd, gmii_rxd                                      : std_logic_vector(7 downto 0);
  signal gmii_tx_en, gmii_tx_er, gmii_rx_dv, gmii_rx_er          : std_logic;
  signal gmii_rx_clk                                             : std_logic;
  signal clkin, clk125, txoutclk_ub, txoutclk, clk125_ub, clk_fr : std_logic;
  signal clk62_5_ub, clk62_5, clkfb, rxoutclk_nb                 : std_logic;
  signal clk2_nbuf                                               : std_logic;

  signal phy_done, mmcm_locked : std_logic;
  signal status                : std_logic_vector(15 downto 0);

  signal gtrefclk_out  : std_logic;
  signal force_reset   : std_logic_vector(0 downto 0);
  signal v_mmcm_locked : std_logic_vector(0 downto 0);
  signal rsti2         : std_logic;

begin

  rst_out <= rsti2;
  rsti2            <= rsti or force_reset(0);
  v_mmcm_locked(0) <= mmcm_locked;

  --ibuf1 : IBUFDS_GTE2 port map(
  --  i   => clk2_p,
  --  ib  => clk2_n,
  --  o   => clk2_nbuf,
  --  ceb => '0'
  --  );

  --bufg2 : BUFG port map(
  --  i => clk2_nbuf,
  --  o => clk2
  --  );
  --bufg2 : BUFG port map(
  --  i => rxoutclk_nb,
  --  o => rxoutclk
  --);

--  clk125_fr <= clk_fr;

  locked <= mmcm_locked;

  eth_usp_gmii_1 : eth_usp_gmii
    port map (
      clk125      => clk125,
      rst         => rsti,
      --gmii_gtx_clk => open,
      gmii_txd    => gmii_txd,
      gmii_tx_en  => gmii_tx_en,
      gmii_tx_er  => gmii_tx_er,
      gmii_rx_clk => clk125,            --? Czy rxoutclk?
      gmii_rxd    => gmii_rxd,
      gmii_rx_dv  => gmii_rx_dv,
      gmii_rx_er  => gmii_rx_er,
      tx_data     => tx_data,
      tx_valid    => tx_valid,
      tx_last     => tx_last,
      tx_error    => tx_error,
      tx_ready    => tx_ready,
      rx_data     => rx_data,
      rx_valid    => rx_valid,
      rx_last     => rx_last,
      rx_error    => rx_error,
      hostbus_in  => hostbus_in,
      hostbus_out => hostbus_out);

  clk125_out <= clk125;
  status_o   <= status;

  --hostbus_out.hostrddata  <= (others => '0');
  --hostbus_out.hostmiimrdy <= '0';

PCS1 : gig_ethernet_pcs_pma_0
  PORT MAP (
    sgmii_clk_r_0 => open,
    sgmii_clk_f_0 => open, 
    sgmii_clk_en_0 => open, 
    clk125_out => clk125, 
    clk312_out => open, 
    rst_125_out => open, 
    refclk625_n => gt_clkn,
    refclk625_p => gt_clkp,
    speed_is_10_100_0 => '0',
    speed_is_100_0 => '0',
    reset => rsti2,
    txn_0 => gt_txn,
    rxn_0 => gt_rxn,
    gmii_txd_0 => gmii_txd,
    gmii_rxd_0 => gmii_rxd,
    txp_0 => gt_txp,
    gmii_rx_dv_0 => gmii_rx_dv,
    gmii_rx_er_0 => gmii_rx_er,
    gmii_isolate_0 => open,
    rxp_0 => gt_rxp,
    signal_detect_0 => '1',
    gmii_tx_en_0 => gmii_tx_en,
    gmii_tx_er_0 => gmii_tx_er,
    configuration_vector_0 => "10000",
    status_vector_0 => status,
    an_adv_config_vector_0 => "1101100000000001",
    an_restart_config_0 => '0',
    an_interrupt_0 => open,
    tx_dly_rdy_1 => '1',
    rx_dly_rdy_1 => '1',
    tx_vtc_rdy_1 => '1',
    rx_vtc_rdy_1 => '1',
    tx_dly_rdy_2 => '1',
    rx_dly_rdy_2 => '1',
    tx_vtc_rdy_2 => '1',
    rx_vtc_rdy_2 => '1',
    tx_dly_rdy_3 => '1',
    rx_dly_rdy_3 => '1',
    tx_vtc_rdy_3 => '1',
    rx_vtc_rdy_3 => '1',
    tx_logic_reset => open,
    rx_logic_reset => open,
    rx_locked => open,
    tx_locked => open,
    tx_bsc_rst_out => open,
    rx_bsc_rst_out => open,
    tx_bs_rst_out => open,
    rx_bs_rst_out => open,
    tx_rst_dly_out => open,
    rx_rst_dly_out => open,
    tx_bsc_en_vtc_out => open,
    rx_bsc_en_vtc_out => open,
    tx_bs_en_vtc_out => open,
    rx_bs_en_vtc_out => open,
    riu_clk_out => open,
    riu_wr_en_out => open,
    tx_pll_clk_out => open,
    rx_pll_clk_out => open,
    tx_rdclk_out => open,
    riu_addr_out => open,
    riu_wr_data_out => open,
    riu_nibble_sel_out => open,
    rx_btval_1 => open,
    rx_btval_2 => open,
    rx_btval_3 => open,
    riu_valid_3 => '0',
    riu_valid_2 => '0',
    riu_valid_1 => '0',
    riu_prsnt_1 => '0',
    riu_prsnt_2 => '0',
    riu_prsnt_3 => '0',
    riu_rddata_3 => (others => '0'),
    riu_rddata_1 => (others => '0'),
    riu_rddata_2 => (others => '0')
  );
  
  vio_0_1 : entity work.vio_0
    port map (
      clk        => clk125,
      probe_in0  => status,
      probe_in1  => v_mmcm_locked,
      probe_out0 => force_reset);

end rtl;

