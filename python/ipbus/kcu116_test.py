import uhal
import sys
import pickle
import time
import struct
#class mytime:
#   def __init__(self):
#     return
#   def sleep(self,delay):
#     bus_delay(delay/1e-9)
#time=mytime()

manager = uhal.ConnectionManager("file://kcu116_conn.xml")
hw = manager.getDevice("dummy.udp.0")
#Configure objects used for IPbus communication
class ipbus(object):
  def __init__(self,hw,node):
    self.hw = hw
    self.node = hw.getNode(node)
    pass
  def write(self,val):
    self.node.write(val)
    self.hw.dispatch()
  def read(self):
    v=self.node.read()
    self.hw.dispatch()
    return v
  def readBlock(self,nwords):
    v=self.node.readBlock(nwords)
    self.hw.dispatch()
    return v

#Configure objects used for IPbus communication
#ID
IDReg=ipbus(hw,"ID")
Buttons=ipbus(hw,"BUTTONS")
Leds=ipbus(hw,"LEDS")

